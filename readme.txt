reference : https://docs.gitlab.com/ee/api/jobs.html

trigger pipeline
curl -X POST -F token=8116718a601bd0ce310bd202f651cf  -F ref=master  https://gitlab.com/api/v4/projects/19838470/trigger/pipeline


to list the manual job by scope
curl --globoff --header "PRIVATE-TOKEN: zMxigJ9zz4B1sq2rGDuW" "https://gitlab.com/api/v4/projects/19838470/pipelines/182397313/jobs?scope[]=manual"

triggre by job id
curl --request POST --header "PRIVATE-TOKEN: zMxigJ9zz4B1sq2rGDuW" "https://gitlab.com/api/v4/projects/19838470/jobs/705547378/play"

Scope of jobs to show. Either one of or an array of the following: created, pending, running, failed, success, canceled, skipped, or manual. All jobs are returned if scope is not provided.
